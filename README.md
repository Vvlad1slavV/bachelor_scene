![scene pic](docs/figures/scene_view.jpg)

# semifinal_bachelor_scene
Репозиторий со сценой для полуфинала олимпиады "Я - профессионал" для бакалавров.  

Для локальной сборки достаточно выполнить, не забыв выполнить при этом `docker login` в gitlab.

```bash
docker login registry.gitlab.com -u <username> -p <token>
```

```bash
docker build --pull -t scene_bachelor_semifinal . --build-arg  BASE_IMG=registry.gitlab.com/beerlab/iprofi2023/problem/bachelor_problem_2023/base:nvidia-latest
```
Для дальнейшего запуска дадим возможность авторизации для X-server извне:
```bash
xhost +local:docker
```

Далее для запуска достаточно склонировать репозиторий, предоставляемый участникам https://gitlab.com/beerlab/iprofi2023/problem/bachelor и запустить:
```bash
docker compose -f docker-compose.nvidia.yml up
```
Или без docker compose:
```bash
docker run -ti scene_bachelor_semifinal:latest
```

Открыть новую bash-сессию в контейнере

```bash
docker exec -ti scene_bachelor_semifinal bash
```

## License
MIT License
